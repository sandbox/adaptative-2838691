Template per item
_________________

### Summary

The module aims to provide a new field formatter for entity reference field. The "Template per item" provides
new template suggestions for each bundle added in a multi valued entity reference field.

Known problems:

Currently it is not possible witch any multi valued field including entity reference filed to have different
template for each item added in the field. Sometime user may require each element from different bundles to have
different look and feel which is not possible with the existing formats. The workaround may be to use "Rendered entity"
field format by using a designated view mode but this does not work with "Block content" entities.

### Configuration

Install this module with usual steps.
- Register hook_theme function in a custom module.
- Use following signature to create your own custom template file:
field_{parent entity type}_{field type}_{field name}_{item bundle type}

For example if you have added a multi valued entity reference field to the paragraph and added one "Block content" item
the them hook will be added like this:

function <YOUR CUSTOM MODULE NAME>_theme() {
  return array(
    'field_paragraph_entity_reference_field_block_component_banner' => array(
      'variables' => array('item' => NULL),
    ),
  );
}

Now you just need to add a field-paragraph-entity-reference-field-block-component-banner.html.twig template file to your
custom theme.

