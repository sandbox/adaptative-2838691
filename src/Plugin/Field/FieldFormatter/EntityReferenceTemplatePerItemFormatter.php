<?php

namespace Drupal\tal_downloads\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
/**
 * Plugin implementation of the 'entity reference label' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_template_per_item",
 *   label = @Translation("Template Per Item"),
 *   description = @Translation("Provide options of template files for each iteam added based on bundle type of the referenced entities."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceTemplatePerItemFormatter extends EntityReferenceEntityFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'TemplatePerItem' => TRUE,
      'view_mode' => 'default'
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['TemplatePerItem'] = array(
      '#title' => t('Use different template for per item bundle.'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('link'),
    );
    $elements['view_mode'] = array(
      '#type' => 'select',
      '#options' => $this->entityDisplayRepository->getViewModeOptions($this->getFieldSetting('target_type')),
      '#title' => t('View mode'),
      '#default_value' => $this->getSetting('view_mode'),
      '#required' => TRUE,
    );

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();
    $summary[] = $this->getSetting('TemplatePerItem') ? t('Use different template for per item bundle.') : t('Don\'t use.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();
    $use_template_per_item = $this->getSetting('TemplatePerItem');
    $template = $items->getFieldDefinition()->get('entity_type');
    $template .= '_' . $items->getFieldDefinition()->get('field_type');
    $template .= '_' . $items->getFieldDefinition()->get('field_name');

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {

      if ($use_template_per_item && !$entity->isNew()) {

        $view_builder = $this->entityTypeManager->getViewBuilder($entity->getEntityTypeId());
        $elements[$delta] = [
          '#theme' => 'field_' . $template . '_' . $entity->bundle(),
          '#item' => $view_builder->view($entity, 'default', $entity->language()->getId()),
        ];
      }

      $elements[$delta]['#cache']['tags'] = $entity->getCacheTags();
    }

    return $elements;
  }

}
